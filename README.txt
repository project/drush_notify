# Drush Notify

This drush "command" demonstrates the power of a drush extension that defines
no new commands of it's own.

Drush Notify currently supports Linux (via libnotify) and OSX (via terminal-notifier),
with fallback support via configuration parameters.

## Installation
On OSX, you must run:
`gem install terminal-notifier`

## (Limited) Documentation via the Command-line

`drush core-global-options`

## Options

### --notify
After the completion of a drush command trigger a popup notification.
This may be used as a one-off flag on the current command, or set in your 
drushrc.php file as a whitelist of commands to notify.

If you specify --notify as a positive number instead of a boolean, that number
will be treated as a number of seconds that command execution must take before
the notification will be approved for delivery.

### --notify-audio
Given the use of --notify, also trigger an audio alert. By default, this is a
robotic voice reciting the message.

## Configuration Options

You can use these Options in-line, but they will rarely change on a given laptop.
Better to add them to a per-OS `drushrc.php` file.

### notify-cmd
Select a command-line tool for the popup notification. May be defined globally or per command.

If specified, this will override default utility integration.

### notify-cmd-audio
Select a command-line tool for the popup notification. May be defined globally or per command.

If specified, this will override default utility integration.

## On By Default

// Always notify on cache clear.
$command_specific['cache-clear']['notify'] = TRUE;
// Always notify w/ audio on site installation.
$command_specific['site-install']['notify'] = TRUE;
$command_specific['site-install']['notify']['notify-audio'] = TRUE;
// Notify on any command that takes longer than 5s to run.
$options['notify'] = 5;

## Customization
If you wish to add additional content to the message, implement
hook_drush_help() for the section 'notify:{command name}'. This will be
appended to other specified messages, or replace the default message.

If the command throws an error, you can replace the error text with
'notify:{command name}:error'.

## API Available

If any of these don't work from your drush command, file an issue at
http://drupal.org/project/issues/drush_notify.

* **drush_notify_send()**: Send a message to notifications center and audio.
* **drush_notify_send_text()**: Send a message to notification center.
* **drush_notify_send_audio()**: Send a message to audio system.

