<?php
/**
 * @file
 * Add system notifications as a new drush option.
 */

/**
 * There are no hooks fired after a command errors out.
 */
register_shutdown_function('drush_notify_shutdown_error');

/**
 * Implements hook_drush_help_alter().
 */
function drush_notify_drush_help_alter(&$command) {
  if ($command['command'] == 'global-options') {
    $command['options']['notify'] = 'Use system notifications to signal command completion. If set to a number, that will be the number of seconds of command execution before the notification will be triggered.';
    $command['options']['notify-audio'] = 'Trigger an audio alert to signal command completion.';

    // Do not include these in options in help option listings.
    if ($command['#brief'] === FALSE) {
      $command['sub-options']['notify']['notify-cmd'] = 'Specify the shell command to trigger the notification.';
      $command['sub-options']['notify']['notify-cmd-audio'] = 'Specify the shell command to trigger the audio notification.';
    }
  }
}

/**
 * Implements hook_drush_help().
 */
function drush_notify_drush_help($section) {
  switch ($section) {
    case 'notify:cache-clear':
      return dt('Caches have been cleared.');
    case 'notify:site-install:error':
      return dt('Failed on site installation');
  }
}

/**
 * Implements hook_drush_exit().
 */
function drush_notify_drush_exit() {
  $cmd = drush_get_command();
  // pm-download is handled via richer data hook.
  if ($cmd['command'] != 'pm-download' && drush_notify_command_allowed($cmd['command'])) {
    $msg = dt("Command '!command' completed.", array('!command' => $cmd['command']));
    drush_notify_send(drush_notify_command_message($cmd['command'], $msg));
  }
}

/**
 * Shutdown function to signal on errors.
 */
function drush_notify_shutdown_error() {
  if (drush_get_option('notify', FALSE) && drush_get_error()) {
    // If the only error is that notify failed, do not try to notify again.
    $log = drush_get_error_log();
    if (count($log) == 1 && array_key_exists('NOTIFY_COMMAND_NOT_FOUND', $log)) {
      return;
    }

    // Send an alert that the command failed.
    $cmd = drush_get_command();
    if (drush_notify_command_allowed($cmd['command'])) {
      $msg = dt("Command '!command' failed.", array('!command' => $cmd['command']));
      drush_notify_send(drush_notify_command_message($cmd['command'] . ':error', $msg));
    }
  }
}

/**
 * Implements hook_drush_pm_post_download().
 */
function drush_notify_drush_pm_post_download($project, $release) {
  // Use system notifications to signal download completion.
  if (drush_notify_command_allowed('pm-download')) {
    $msg = dt('Project !project (!version) downloaded to !install.', array(
      '!project' => $project['name'],
      '!version' => $release['version'],
      '!install' => $project['project_install_location'],
    ));
    drush_notify_send(drush_notify_command_message('pm-download', $msg));
  }
}

/**
 * Determine the message to send on command completion.
 *
 * @param string $command
 *   Name of the Drush command for which we check message overrides.
 * @param string $default
 *   (Default: NULL) Default message to use if there are not notification message overrides.
 *
 * @return string
 *   Message to use for notification.
 */
function drush_notify_command_message($command, $default = NULL) {
  if ($msg = drush_command_invoke_all('drush_help', 'notify:' . $command)) {
    $msg = implode("\n", $msg);
  }
  else {
    $msg = $default ? $default : $msg = $command . ': No news is good news.';
  }

  return $msg;
}

/**
 * Prepares and dispatches notifications to delivery mechanisms.
 *
 * You may avoid routing a message to secondary messaging mechanisms (e.g. audio),
 * by direct use of the delivery functions.
 *
 * @param string $msg
 *   Message to send via notification.
 */
function drush_notify_send($msg) {
  drush_notify_send_text($msg);
  if (drush_get_option('notify-audio', FALSE)) {
    drush_notify_send_audio($msg);
  }
}

/**
 * Send text-based system notification.
 *
 * This is the automatic, default behavior. It is intended for use with tools
 * such as libnotify in Linux and Growl on OSX.
 *
 * @param string $msg
 *   Message text for delivery.
 *
 * @return bool
 *   TRUE on success, FALSE on failure
 */
function drush_notify_send_text($msg) {
  $override = drush_get_option('notify-cmd', FALSE);

  if (!empty($override)) {
    $cmd = $override;
  }
  else {
    switch (PHP_OS) {
      case 'Darwin':
        $cmd = 'terminal-notifier -message %s -title Drush';
        break;
      case 'Linux':
      default:
        $icon = drush_normalize_path(DRUSH_BASE_PATH . '/drush_logo-black.png');
        $cmd = "notify-send %s -i $icon";
        break;
    }
  }

  if (!drush_shell_exec($cmd, $msg)) {
    return drush_set_error('NOTIFY_COMMAND_NOT_FOUND', dt('The third party notification utility failed.'));
  }

  return TRUE;
}

/**
 * Send an audio-based system notification.
 *
 * This function is only automatically invoked with the additional use of the
 * --notify-audio flag or configuration state.
 *
 * @param $msg
 *   Message for audio recital.
 *
 * @return bool
 *   TRUE on success, FALSE on failure
 */
function drush_notify_send_audio($msg) {
  $override = drush_get_option('notify-cmd-audio', FALSE);

  if (!empty($override)) {
    $cmd = $override;
  }
  else {
    switch (PHP_OS) {
      case 'Darwin':
        $cmd = 'say %s';
        break;
      case 'Linux':
      default:
        $cmd = drush_get_option('notify-cmd-audio', 'spd-say') . ' %s';
    }
  }

  if (!drush_shell_exec($cmd, $msg)) {
    return drush_set_error('NOTIFY_COMMAND_NOT_FOUND', dt('The third party notification utility failed.'));
  }
}

/**
 * Identify if the given Drush Command should trigger a notification.
 *
 * @param $command
 *   Name of the command.
 *
 * @return
 *   Boolean
 */
function drush_notify_command_allowed($command) {
  $notify = drush_get_option('notify', FALSE);
  $execution = time() - $_SERVER['REQUEST_TIME'];

  drush_log(dt('Before --notify trigger, command took !execution seconds.',
    array('!execution' => $execution)
  ));

  return ($notify === TRUE ||
    (is_numeric($notify) && $notify > 0 && $execution > $notify));
}

